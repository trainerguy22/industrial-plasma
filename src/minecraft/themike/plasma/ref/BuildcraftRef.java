package themike.plasma.ref;

import java.util.logging.Level;

import themike.plasma.core.IndustrialPlasma;

public class BuildcraftRef {
	
	private static Class bc = null;
	
	public static void setClass(Class cls) {
		bc = cls;
	}
	
	public static Object getValue(String value) {
		if(bc != null) {
			try {
				return bc.getField(value).get(null);
			}catch(Exception e) {
                IndustrialPlasma.log(Level.SEVERE, "Error getting Buildcraft field!");
				IndustrialPlasma.log(e);
			}
		}
		return null;
	}

}
