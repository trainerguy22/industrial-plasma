package themike.plasma.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.world.World;
import themike.plasma.tileentity.TileEntityIncinerator;
import themike.plasma.tileentity.TileEntityMachine;

public class BlockExperienceIncinerator extends BlockContainer {

    public BlockExperienceIncinerator(int defid) {
        super(defid, Material.rock);
    }

    @Override
    public TileEntityMachine createNewTileEntity(World world) {
        return new TileEntityIncinerator();
    }

    @Override
    public void registerIcons(IconRegister register) {
        this.blockIcon = register.registerIcon("industrialplasma:incinerator_1");
    }
}
