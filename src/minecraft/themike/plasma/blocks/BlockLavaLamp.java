package themike.plasma.blocks;

import java.util.List;
import java.util.Random;
import java.util.logging.Handler;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import themike.core.block.BlockBase;
import themike.plasma.core.IndustrialPlasma;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;

public class BlockLavaLamp extends BlockBase {

	public BlockLavaLamp(int defid) {
		super(defid, Material.glass, "industrialplasma", "lavaLamp");
		setHardness(0.3F);
		setStepSound(Block.soundGlassFootstep);
	}
	
    @Override
    public boolean canProvidePower() {
        return true;
    }
    
    @Override
    public boolean isBlockSolidOnSide(World world, int x, int y, int z, ForgeDirection side) {
        return true;
    }
    
	@Override
    public int getLightValue(IBlockAccess world, int x, int y, int z) {
		return 15;
	}
	
	@Override
	public int damageDropped(int metadata){
		return 0;
	}
	
    private static boolean isBlockSingleSlab(int par0) {
        return par0 == IndustrialPlasma.slabGlass.blockID;
    }
    
	@Override
	public void getSubBlocks(int par1, CreativeTabs tab, List subItems){
		subItems.add(new ItemStack(this, 1, 0));
	}

}
