package themike.plasma.blocks;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import themike.core.block.BlockBase;
import themike.plasma.core.IndustrialPlasma;
import themike.plasma.items.ItemBlocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class Blocks extends BlockBase {
	
	private String[] blocks = new String[]{"stoneBevel"};
	private Icon[] icons = new Icon[blocks.length];

	public Blocks(int defid) {
		super(defid, Material.iron, "industrialplasma", "blocks", ItemBlocks.class);
		setHardness(5.0F);
		setResistance(10.0F);
		setStepSound(Block.soundMetalFootstep);
	}

	
	@Override
	public int damageDropped(int metadata){
		return metadata;
	}
	
	@Override
	public void getSubBlocks(int ID, CreativeTabs tab, List subItems) {
		for(int par1 = 0; par1 < blocks.length; par1++) {
			subItems.add(new ItemStack(this, 1, par1));
		}
	}
	
	@Override
	public Icon getIcon(int side, int meta) {
		return icons[meta];
	
	}

    @Override
    public void registerIcons(IconRegister register) {
    	int count = 0;
        for(String block : blocks) {
        	icons[count] = register.registerIcon("industrialplasma:" + block);
        }
    }

}
