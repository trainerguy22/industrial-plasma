package themike.plasma.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import themike.core.block.SlabBase;
import themike.plasma.core.IndustrialPlasma;

public class SlabGlass extends SlabBase {

	public SlabGlass(int par1, boolean doubl) {
		super(par1, doubl, Material.rock, "industrialplasma", "slabGlass");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister reg) {
        this.blockIcon = reg.registerIcon("glass");
	}

}
