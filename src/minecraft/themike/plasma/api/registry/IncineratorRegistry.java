package themike.plasma.api.registry;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import themike.plasma.api.recipe.IncineratorRecipe;

import java.util.HashMap;

public final class IncineratorRegistry {

    static HashMap<Integer, Integer> bottleLookup = new HashMap<Integer, Integer>();
    static HashMap<Integer, IncineratorRecipe> recipeLookup = new HashMap<Integer, IncineratorRecipe>();

    /**
     *
     * @param id Block/item ID
     * @return if id is a valid xp container
     */
    public static boolean isXpContainer(int id) {
        return bottleLookup.containsKey(id);
    }

    /**
     * @param input Block/item ID of input
     * @param output Block/item ID of output
     */
    public static void registerXpContainer(int input, int output) {
        if (bottleLookup.containsKey(input)) return;
        bottleLookup.put(input, output);
    }

    /**
     * Check if the block/item ID is incineratable
     * @param id Block/item id
     * @return if the id is incineratable
     */
    public static boolean isIncineratable(int id) {
        return recipeLookup.containsKey(id);
    }

    /**
     * @param id Block/item id
     * @return recipe for the incinerator
     */
    public static IncineratorRecipe getRecipe(int id) {
        return recipeLookup.get(id);
    }

    public static void registerIncineratorRecipe(ItemStack item, int amountXp, int euPerTick, int processDuration) {
        recipeLookup.put(item.getItem().itemID, new IncineratorRecipe(item, amountXp, euPerTick, processDuration));
    }

    public static void registerIncineratorRecipe(IncineratorRecipe recipe) {
        recipeLookup.put(recipe.getInput().getItem().itemID, recipe);
    }

    public static ItemStack getXpRecipe(int id) {
        return new ItemStack(bottleLookup.get(id), 1, 0);
    }
}
