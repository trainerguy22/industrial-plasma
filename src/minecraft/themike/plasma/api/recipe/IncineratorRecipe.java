package themike.plasma.api.recipe;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class IncineratorRecipe {
    private Item input;
    private int amountXp;
    private int euPerTick;
    private int processDuration;

    public IncineratorRecipe(ItemStack input, int amountXp, int euPerTick, int processDuration) {
        this.euPerTick = euPerTick;
        this.processDuration = processDuration;
        this.input = input.getItem();
        this.amountXp = amountXp;
    }

    public ItemStack getInput() {
        return new ItemStack(input);
    }

    public int getAmountXp() {
        return amountXp;
    }

    public int getEuPerTick() {
        return euPerTick;
    }

    public int getProcessDuration() {
        return processDuration;
    }
}
