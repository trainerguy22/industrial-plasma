package themike.plasma.event;

import java.util.logging.Level;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.IPlayerTracker;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import themike.plasma.core.IndustrialPlasma;
import net.minecraftforge.client.event.sound.SoundLoadEvent;
import net.minecraftforge.event.ForgeSubscribe;

public class EventHandler implements IPlayerTracker {
	
	@ForgeSubscribe
	public void onSoundLoad(SoundLoadEvent event) {
		
		IndustrialPlasma.log(Level.INFO, "Trying to load sounds.");
		try {
			event.manager.soundPoolSounds.addSound("res/plasma/sounds/tree.ogg", 
					this.getClass().getResource("/res/plasma/sounds/tree.ogg"));
			IndustrialPlasma.log(Level.WARNING, "Loaded sounds!");
		} catch(Exception e) {
			IndustrialPlasma.log(Level.WARNING, "Failed to load sounds!");
		}
		
	}

    @Override
    public void onPlayerLogin(EntityPlayer player) {
        if (FMLCommonHandler.instance().getEffectiveSide() != Side.CLIENT) return;

    }

    @Override
    public void onPlayerLogout(EntityPlayer player) {
    }

    @Override
    public void onPlayerChangedDimension(EntityPlayer player) {
    }

    @Override
    public void onPlayerRespawn(EntityPlayer player) {
    }
}
