package themike.plasma.core;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import cpw.mods.fml.common.registry.GameRegistry;

public class IPRecipe {
	
	public static void init() {
		
		GameRegistry.addRecipe(new ItemStack(IndustrialPlasma.heavyAxe, 1), new Object[]{
			"XXX", "XIX", " I ", 'X', Item.ingotIron, 'I', Item.stick
		});
		
		GameRegistry.addRecipe(new ItemStack(IndustrialPlasma.slabGlass, 12), new Object[]{
			" X ", "IXI", " X ", 'I', Item.ingotIron, 'X', Block.glass
		});
		
		GameRegistry.addRecipe(new ItemStack(IndustrialPlasma.blockLavaLamp, 4), new Object[]{
			" X ", "XIX", " X ", 'I', Item.bucketLava, 'X', Block.thinGlass
		});
		
		GameRegistry.addRecipe(new ItemStack(IndustrialPlasma.blocks, 2, 0), new Object[]{
			"X ", " X", 'X', Block.stone
		});
		
	}

}
