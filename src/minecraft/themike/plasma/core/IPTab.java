package themike.plasma.core;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class IPTab extends CreativeTabs {

	public String tabName;
	
	public IPTab(String label) {
		super(label);
		tabName = label;
	}
	
	@Override
	public ItemStack getIconItemStack(){
		ItemStack icon = new ItemStack(IndustrialPlasma.heavyAxe, 1);
		return icon;
	}

}
