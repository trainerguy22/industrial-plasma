package themike.plasma.core;

import java.util.logging.Level;
import java.util.logging.Logger;

import cpw.mods.fml.common.FMLLog;
import net.minecraft.item.ItemStack;
import themike.core.block.SlabBase;
import themike.plasma.api.registry.IncineratorRegistry;

import themike.plasma.blocks.BlockExperienceIncinerator;
import themike.plasma.blocks.BlockLavaLamp;
import themike.plasma.blocks.Blocks;
import themike.plasma.blocks.SlabGlass;
import themike.plasma.event.EventHandler;
import themike.plasma.items.ToolChainsaw;
import themike.plasma.items.ToolHeavyAxe;
import themike.plasma.packet.PacketHandler;
import themike.plasma.proxy.CommonProxy;
import themike.plasma.ref.BuildcraftRef;

import net.minecraft.block.Block;
import net.minecraft.block.BlockHalfSlab;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.EnumHelper;
import net.minecraftforge.common.MinecraftForge;

import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PostInit;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@NetworkMod(clientSideRequired = true, serverSideRequired = false, channels=("IndustrialPlasma"), packetHandler = PacketHandler.class)
@Mod(modid = "IndustrialPlasma", name = "Industrial Plasma", version = "0.3 BETA")
public class IndustrialPlasma {
	
	@Instance("IndustrialPlasma")
	public static IndustrialPlasma instance;
	
	public static boolean thermalExpansion = false;
	
	public static final String RES_DIR = "/res/plasma/";
	public static final String LANG_DIR = RES_DIR + "lang/";
	public static final String GUI_DIR = RES_DIR + "gui/";
	public static final String ARMOR_DIR = RES_DIR + "armor/";
	public static final String TEX_DIR = RES_DIR + "textures/";
    public static final String PACKET_CHANNEL = "indusplasma";
    public static final String VERSIONFILE_URL = "http://localhost/ipversion.json";
	
	public final static CreativeTabs tab = new IPTab("atomicTab");

	public static Block blockLavaLamp;
	public static Block blocks;
	public static SlabBase slabGlass;
    public static BlockExperienceIncinerator experienceIncinerator;
	
	public static Item miscItems;

	public static Item heavyAxe;
	public static Item chainsaw;
		
	@SidedProxy(clientSide = "themike.plasma.proxy.ClientProxy", serverSide = "themike.plasma.proxy.CommonProxy")
	public static CommonProxy proxy;
	
    public static Configuration config;
    private static Logger logger = Logger.getLogger("IndustrialPlasma");;
    private static boolean debug;

    @PreInit
	public void preInit(FMLPreInitializationEvent event) {

        logger.setParent(FMLLog.getLogger());

		LanguageRegistry.instance().loadLocalization(LANG_DIR + "en_US.xml", "en_US", true);
		MinecraftForge.EVENT_BUS.register(new EventHandler());
		
		config = new Configuration(event.getSuggestedConfigurationFile());
			
		config.load();
		
		heavyAxe = new ToolHeavyAxe(config.getItem("Heavy Axe", 8501).getInt(8501), EnumToolMaterial.IRON);
		chainsaw = new ToolChainsaw(config.getItem("Redstone Chainsaw", 8502).getInt(8502), EnumHelper.addToolMaterial("Chainsaw", 3, 500, 14.0F, 3, 0));

        debug = config.get("general", "debug", false).getBoolean(false);

        blockLavaLamp = new BlockLavaLamp(config.getBlock("Lava Lamp", 3451).getInt(3451));
        blocks = new Blocks(config.getBlock("Blocks", 3454).getInt(3454));
        experienceIncinerator = new BlockExperienceIncinerator(config.getBlock("Experience Incinerator", 3452).getInt(3452));
        slabGlass = (SlabBase) new SlabGlass(config.getBlock("Glass Step", 3453).getInt(3453), false);
		
		config.save();
	}
	
	@Init
	public void init(FMLInitializationEvent event) {
										
		proxy.registerClientTextures();
		IPRecipe.init();
				
		NetworkRegistry.instance().registerGuiHandler(this, new CommonProxy());
		
		this.blockLavaLamp.setCreativeTab(tab);
		this.blocks.setCreativeTab(tab);
		this.slabGlass.setCreativeTab(tab);

	}
	
	@PostInit
	public void postInit(FMLPostInitializationEvent event) {
		initAddons();
        IncineratorRegistry.registerXpContainer(Item.glassBottle.itemID, Item.expBottle.itemID);
        IncineratorRegistry.registerIncineratorRecipe(new ItemStack(net.minecraft.block.Block.dirt), 1, 0, 20);
    }

    public static void log(String message) {
        log(Level.INFO, message);
    }

    public static void log(Level level, String message) {
        logger.log(level, message);
    }

    public static void debugPrint(String message) {
        if (debug)
            logger.log(Level.INFO, "[DEBUG] " + message);
    }

    public static void log(Level level, Throwable ex) {
        log(level, "This is a bug, report it to TheMike:");
        logger.log(level, ex.getClass().getName() + ": " + ex.getMessage());
        for (StackTraceElement s : ex.getStackTrace()) {
            logger.log(level, "   at " + s.toString());
        }
        Throwable cause = ex.getCause();
        if (cause != null) {
            log(level, "Caused by " + cause.getClass().getName() + ": " + cause.getMessage());
            for (StackTraceElement s : ex.getStackTrace()) {
                logger.log(level, "   at " + s.toString());
            }
        }
    }

    public static void log(Throwable ex) {
        log(Level.SEVERE, ex);
    }

	public void initAddons() {
		
		if(Loader.isModLoaded("BuildCraft|Core")){
			try{
				BuildcraftRef.setClass(Class.forName("buildcraft.BuildCraftCore")); 
			}catch (Exception e){
                logger.log(Level.WARNING, "Missing Buildcraft! Your either using a wrong version, or you didn't read the install guide.");
                this.log(e);
			}
		}else{
            logger.log(Level.WARNING, "Missing Buildcraft! Your either using a wrong version, or you didn't read the install guide.");
		}
		if (Loader.isModLoaded("ThermalExpansion")) {
			try{
				thermalExpansion = true;
			}catch(Exception e){
				logger.log(Level.WARNING, "Missing Thermal Expansion! No Chainsaw for you!");
				this.log(e);
			}
		}else{
			logger.log(Level.WARNING, "Missing Thermal Expansion! No Chainsaw for you!");
		}
		
	}



}
