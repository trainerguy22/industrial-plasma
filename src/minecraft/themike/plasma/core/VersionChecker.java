package themike.plasma.core;

import cpw.mods.fml.common.Mod;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Level;

public class VersionChecker implements Runnable {

    public JSONObject data;

    public static void doCheck() {
        new Thread(new VersionChecker()).start();
    }

    @Override
    public void run() {
        data = getData();
        if (data == null) return;
        if (!IndustrialPlasma.class.getAnnotation(Mod.class).version().equals(data.getString("lastestVersion"))) {
            data.put("isLatest", false);
            IndustrialPlasma.log(Level.WARNING, "Industrial Plasma is outdated, lastest version is " + data.getString("lastestVersion"));
            IndustrialPlasma.log(Level.WARNING, data.getString("changelog"));
        } else {
            data.put("isLatest", true);
        }
        // TODO: add code to display a message

    }

    private JSONObject getData() {
        try {
            HttpURLConnection con = (HttpURLConnection) new URL(IndustrialPlasma.VERSIONFILE_URL).openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Length", "0");
            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);

            InputStream is = con.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }
            rd.close();
            return new JSONObject(response.toString());

        } catch (Exception ex) {
            IndustrialPlasma.log(Level.WARNING, "Version Check failed");
            IndustrialPlasma.log(Level.WARNING, ex);
        }
        return null;
    }
}
