package themike.plasma.items;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.stats.StatList;
import net.minecraft.world.World;
import themike.plasma.core.IndustrialPlasma;
import thermalexpansion.api.item.IChargeableItem;
import thermalexpansion.api.item.ItemRegistry;

public class ToolChainsaw extends ToolHeavyAxe implements IChargeableItem {
	
	private final String textureName;
	
	public ToolChainsaw(int par1, EnumToolMaterial par3EnumToolMaterial) {
		super(par1, par3EnumToolMaterial);
		setUnlocalizedName("chainsaw");
		textureName = "chainsaw";
	}

	@Override
	public float receiveEnergy(ItemStack item, float energy, boolean doReceive) {
		if(doReceive && energy >= 4) {
			if(item.getItemDamage() + 4 >= this.getMaxDamage()) {
				item.setItemDamage(item.getItemDamage() + 4);
				return energy - 4.0F;
			} else {
				int damage = item.getItemDamage();
				item.setItemDamage(this.getMaxDamage());
				return (energy - 4.0F) + ((damage + 4) - this.getMaxDamage());
			}
		}
		return 0;
	}

	@Override
	public float transferEnergy(ItemStack item, float energy, boolean doTransfer) {
		return 0;
	}

	@Override
	public float getEnergyStored(ItemStack item) {
		return item.getItemDamage();
	}

	@Override
	public float getMaxEnergyStored(ItemStack item) {
		return this.getMaxDamage();
	}

	@Override
	public boolean onBlockDestroyed(ItemStack stack, World world, int ID, int x, int y, int z, EntityLiving player){
        if (ID != 0 && Block.blocksList[ID].isLeaves(world, x, y, z)) {
                if (new Random().nextInt(7) == 2) {
                    float f = 0.7F;
                    double d0 = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
                    double d1 = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
                    double d2 = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
                    EntityItem entityitem = new EntityItem(world, (double)x + d0, (double)y + d1, (double)z + d2, new ItemStack(Item.appleRed));
                    entityitem.delayBeforeCanPickup = 10;
                    world.spawnEntityInWorld(entityitem);
                    stack.damageItem(1, player);
                }
        }else if(ID != 0 && Block.blocksList[ID].isWood(world, x, y, z) && IndustrialPlasma.thermalExpansion) {
            if (new Random().nextInt(7) == 3) {
                float f = 0.7F;
                double d0 = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
                double d1 = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
                double d2 = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
                EntityItem entityitem = new EntityItem(world, (double)x + d0, (double)y + d1, (double)z + d2, ItemRegistry.getItem("sawdust", 1));
                entityitem.delayBeforeCanPickup = 10;
                world.spawnEntityInWorld(entityitem);
                stack.damageItem(1, player);
            }
        }
		return true;
	}

}
