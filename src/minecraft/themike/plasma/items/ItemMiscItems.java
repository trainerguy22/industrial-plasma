package themike.plasma.items;

import java.util.List;
import java.util.Random;

import themike.plasma.core.IndustrialPlasma;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemMiscItems extends Item {
	public static final String[] itemNames = new String[]{"sticky", "board", "basic", "advanced", "elite"};
	
	public ItemMiscItems(int par1) {
		super(par1);
		this.hasSubtypes = true;
		this.setCreativeTab(IndustrialPlasma.tab);
		this.setUnlocalizedName("miscItems");
	}
	
	@Override
	public String getUnlocalizedName(ItemStack stack){
		if(stack.getItemDamage() >= 0 && stack.getItemDamage() < itemNames.length){
			int var1 = stack.getItemDamage();
			return this.getUnlocalizedName() + itemNames[var1];
		}
		return this.getUnlocalizedName();
	}
	
	@Override
	public void getSubItems(int ID, CreativeTabs tabs, List list){
		for(int meta = 0; meta < itemNames.length; meta++){
			list.add(new ItemStack(ID, 1, meta));
		}
	}

}
