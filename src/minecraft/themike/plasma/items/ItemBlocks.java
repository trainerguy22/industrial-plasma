package themike.plasma.items;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemBlocks extends ItemBlock {

	private String[] blocks = new String[]{"stoneBevel"};
	
	public ItemBlocks(int par1) {
		super(par1);
		this.hasSubtypes = true;
	}
	
	@Override
	public String getUnlocalizedName(ItemStack stack){
		if(stack.getItemDamage() >= 0 && stack.getItemDamage() < blocks.length){
			int var1 = stack.getItemDamage();
			return "tile" + blocks[var1];
		}
		return this.getUnlocalizedName();
	}
	
	@Override
	public int getMetadata(int damage){
		return damage;
	}

}
