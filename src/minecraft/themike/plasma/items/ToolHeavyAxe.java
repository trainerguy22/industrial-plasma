package themike.plasma.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLog;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.world.World;
import themike.core.packet.PacketSound;
import themike.plasma.core.IndustrialPlasma;
import themike.plasma.packet.PacketHandler;

import java.util.Random;

public class ToolHeavyAxe extends ItemTool {
	
	public static Block[] effective = new Block[]{Block.wood, };
	private final String textureName;
	
	public ToolHeavyAxe(int par1, EnumToolMaterial par3EnumToolMaterial) {
		super(par1, 4, par3EnumToolMaterial, effective);
		setCreativeTab(IndustrialPlasma.tab);
		setUnlocalizedName("heavyAxe");
		textureName = "heavyAxe";
	}
	
	@Override
	public boolean onBlockDestroyed(ItemStack stack, World world, int ID, int x, int y, int z, EntityLiving player){
        if (ID != 0 && Block.blocksList[ID].isLeaves(world, x, y, z)) {
                if (new Random().nextInt(7) == 2) {
                    float f = 0.7F;
                    double d0 = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
                    double d1 = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
                    double d2 = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
                    EntityItem entityitem = new EntityItem(world, (double)x + d0, (double)y + d1, (double)z + d2, new ItemStack(Item.appleRed));
                    entityitem.delayBeforeCanPickup = 10;
                    world.spawnEntityInWorld(entityitem);
                    stack.damageItem(1, player);
                }
        }
		return true;
	}
	
	@Override
	public boolean onBlockStartBreak(ItemStack stack, final int x, final int y, final int z, EntityPlayer player) {

		if (!player.worldObj.isRemote) {
			
			if(Block.blocksList[player.worldObj.getBlockId(x, y, z)] instanceof BlockLog
					&& Block.blocksList[player.worldObj.getBlockId(x, y + 1, z)] instanceof BlockLog
					&& Block.blocksList[player.worldObj.getBlockId(x, y + 2, z)] instanceof BlockLog) {
				PacketSound sound = new PacketSound("res.plasma.sounds.tree", 1.5F, 1.0F);
				PacketHandler.sound.sendToAllPlayersAround(sound, (int) player.posX, (int) player.posY + 1, (int) player.posZ, 10, player.dimension);
			}

            Block block = Block.blocksList[player.worldObj.getBlockId(x, y, z)];
			if((block != null) && !(block instanceof BlockLog)){
				int metadata = player.worldObj.getBlockMetadata(x, y, z);
				block.harvestBlock(player.worldObj, player, x, y, z, metadata);
                player.worldObj.setBlock(x, y, z, 0);
			}

            int height = y;
			while (Block.blocksList[player.worldObj.getBlockId(x, ++height, z)] instanceof BlockLog) {
                int metadata = player.worldObj.getBlockMetadata(x, height, z);
                if (!player.capabilities.isCreativeMode) {
                	Block.blocksList[player.worldObj.getBlockId(x, height, z)].harvestBlock(player.worldObj, player, x, height, z, metadata);
                    stack.damageItem(1, player);
                }
                player.worldObj.setBlock(x, height, z, 0);
			}

		}
		
		return super.onBlockStartBreak(stack, x, y, z, player);
	}
	
    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister register) {
        this.itemIcon = register.registerIcon("industrialplasma:" + textureName);
    }
	
}
