package themike.plasma.tileentity;

import buildcraft.api.power.IPowerProvider;
import buildcraft.api.power.IPowerReceptor;
import buildcraft.api.power.PowerFramework;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import themike.plasma.api.recipe.IncineratorRecipe;
import themike.plasma.api.registry.IncineratorRegistry;
import themike.plasma.core.IndustrialPlasma;
import themike.plasma.event.IncineratorProcessDoneEvent;

public class TileEntityIncinerator extends TileEntityMachine implements IPowerReceptor, IInventory {

    private static final int SLOT_INPUT = 0;
    private static final int SLOT_CONTAINER = 1;
    private static final int SLOT_OUTPUT = 2;
    IPowerProvider provider;
    private int progress = 0;
    private int maxProgress = 0;
    private IncineratorRecipe recipe;

    public TileEntityIncinerator() {
        provider = PowerFramework.currentFramework.createPowerProvider();
        provider.configure (100, 1, (int) 10000 /* Max Input */, 100, (int) 10000 /* Max Storage */);
    }

    @Override
    public void setPowerProvider(IPowerProvider provider) {
        this.provider = provider;
    }

    @Override
    public IPowerProvider getPowerProvider() {
        return provider;
    }

    @Override
    public void doWork() {

    }

    @Override
    public int powerRequest() {
        return (int) (provider.getMaxEnergyStored() - provider.getEnergyStored());
    }


    @Override
    public int getSizeInventory() {
        return 3;
    }

    @Override
    public String getInvName() {
        return "Container.Incinerator";
    }

    @Override
    public ItemStack getStackInSlot(int i) {
        //if (i == SLOT_OUTPUT) //output
            return super.getStackInSlot(i);
        //return null;
    }

    @Override
    public void setInventorySlotContents(int slot, ItemStack itemStack) {
        super.setInventorySlotContents(slot, itemStack);
        if (!canProcess()) {
            stop(false);
        }
    }

    @Override
    public boolean canPutInSlot(int slot, ItemStack itemStack) {
        //return slot == 0;
        if (slot == SLOT_OUTPUT) // Output slot
            return false;
        if (slot == SLOT_INPUT) // Input
            return !IncineratorRegistry.isXpContainer(itemStack.itemID);
        if (slot == SLOT_CONTAINER)
            return IncineratorRegistry.isXpContainer(itemStack.itemID);
        return false;
    }

    @Override
    public void updateEntity() {
        if ((!isProcessing()) && canProcess()) {
            startProcess();
        } else if (isProcessing()) {
            float used = provider.useEnergy(recipe.getEuPerTick(), recipe.getEuPerTick(), false);
            if (used == recipe.getEuPerTick()) {
                // only use energy if avaible
                provider.useEnergy(recipe.getEuPerTick(), recipe.getEuPerTick(), true);
                progress++;
                if (progress >= maxProgress) {
                    if (slots[SLOT_OUTPUT] == null)
                        slots[SLOT_OUTPUT] = IncineratorRegistry.getXpRecipe(slots[SLOT_CONTAINER].itemID).copy();
                    else
                        slots[SLOT_OUTPUT].stackSize++;
                    stop(true);
                    MinecraftForge.EVENT_BUS.post(new IncineratorProcessDoneEvent());
                }
            }
        } else {

        }
    }

    private void stop(boolean success) {
        if (!isProcessing()) return;
        IndustrialPlasma.debugPrint("Stopped processing");
        if (success) {
            slots[SLOT_CONTAINER].stackSize--;
            slots[SLOT_INPUT].stackSize--;
        }

        if (slots[SLOT_CONTAINER].stackSize <= 0)
            slots[SLOT_CONTAINER] = null;

        if (slots[SLOT_INPUT].stackSize <= 0)
            slots[SLOT_INPUT] = null;

        progress = 0;
        maxProgress = 0;
        recipe = null;
    }

    private void startProcess() {
        IndustrialPlasma.debugPrint("Starting processing...");
        recipe = IncineratorRegistry.getRecipe(slots[SLOT_INPUT].itemID);
        progress = 0;
        maxProgress = recipe.getProcessDuration();
    }

    private boolean canProcess() {
        if (slots[SLOT_INPUT] == null)
            return false;

        if (slots[SLOT_CONTAINER] == null)
            return false;

        if (!IncineratorRegistry.isIncineratable(slots[SLOT_INPUT].itemID))
            return false;

        IndustrialPlasma.debugPrint("Valid Recipe...");
        return !isProcessing();
    }

    public boolean isProcessing() {
        return maxProgress != 0;
    }

	@Override
	public boolean isInvNameLocalized() {
		return true;
	}

	@Override
	public boolean isStackValidForSlot(int i, ItemStack itemstack) {
		return true;
	}


}
